<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Model;

use Kowal\Cargonizer\Api\Data\CargonizerInterface;
use Magento\Framework\Model\AbstractModel;

class Cargonizer extends AbstractModel implements CargonizerInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\Cargonizer\Model\ResourceModel\Cargonizer::class);
    }

    /**
     * @inheritDoc
     */
    public function getCargonizerId()
    {
        return $this->getData(self::CARGONIZER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCargonizerId($cargonizerId)
    {
        return $this->setData(self::CARGONIZER_ID, $cargonizerId);
    }

    /**
     * @inheritDoc
     */
    public function getCargonizerProducts()
    {
        return $this->getData(self::CARGONIZER_PRODUCTS);
    }

    /**
     * @inheritDoc
     */
    public function setCargonizerProducts($cargonizerProducts)
    {
        return $this->setData(self::CARGONIZER_PRODUCTS, $cargonizerProducts);
    }

    /**
     * @inheritDoc
     */
    public function getMagentoShippingMethod()
    {
        return $this->getData(self::MAGENTO_SHIPPING_METHOD);
    }

    /**
     * @inheritDoc
     */
    public function setMagentoShippingMethod($magentoShippingMethod)
    {
        return $this->setData(self::MAGENTO_SHIPPING_METHOD, $magentoShippingMethod);
    }
}

