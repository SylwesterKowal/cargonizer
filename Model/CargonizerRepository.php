<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Model;

use Kowal\Cargonizer\Api\CargonizerRepositoryInterface;
use Kowal\Cargonizer\Api\Data\CargonizerInterface;
use Kowal\Cargonizer\Api\Data\CargonizerInterfaceFactory;
use Kowal\Cargonizer\Api\Data\CargonizerSearchResultsInterfaceFactory;
use Kowal\Cargonizer\Model\ResourceModel\Cargonizer as ResourceCargonizer;
use Kowal\Cargonizer\Model\ResourceModel\Cargonizer\CollectionFactory as CargonizerCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class CargonizerRepository implements CargonizerRepositoryInterface
{

    /**
     * @var ResourceCargonizer
     */
    protected $resource;

    /**
     * @var CargonizerCollectionFactory
     */
    protected $cargonizerCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var Cargonizer
     */
    protected $searchResultsFactory;

    /**
     * @var CargonizerInterfaceFactory
     */
    protected $cargonizerFactory;


    /**
     * @param ResourceCargonizer $resource
     * @param CargonizerInterfaceFactory $cargonizerFactory
     * @param CargonizerCollectionFactory $cargonizerCollectionFactory
     * @param CargonizerSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceCargonizer $resource,
        CargonizerInterfaceFactory $cargonizerFactory,
        CargonizerCollectionFactory $cargonizerCollectionFactory,
        CargonizerSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->cargonizerFactory = $cargonizerFactory;
        $this->cargonizerCollectionFactory = $cargonizerCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(CargonizerInterface $cargonizer)
    {
        try {
            $this->resource->save($cargonizer);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the cargonizer: %1',
                $exception->getMessage()
            ));
        }
        return $cargonizer;
    }

    /**
     * @inheritDoc
     */
    public function get($cargonizerId)
    {
        $cargonizer = $this->cargonizerFactory->create();
        $this->resource->load($cargonizer, $cargonizerId);
        if (!$cargonizer->getId()) {
            throw new NoSuchEntityException(__('Cargonizer with id "%1" does not exist.', $cargonizerId));
        }
        return $cargonizer;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->cargonizerCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(CargonizerInterface $cargonizer)
    {
        try {
            $cargonizerModel = $this->cargonizerFactory->create();
            $this->resource->load($cargonizerModel, $cargonizer->getCargonizerId());
            $this->resource->delete($cargonizerModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Cargonizer: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($cargonizerId)
    {
        return $this->delete($this->get($cargonizerId));
    }
}

