<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Model\Config\Source;

class Statuses implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\Collection  $collectionStatuses
    ) {
        $this->collectionStatuses = $collectionStatuses;
    }

    public function toOptionArray()
    {
        return  $this->collectionStatuses->toOptionArray();
    }

    public function toArray()
    {
        return $this->getShippingMethodArr();
    }



    public function getShippingMethods() {

        $activeCarriers = $this->shippingAllmethods->toOptionArray();
        foreach($activeCarriers as $code => $shipping) {

            $methods[] = $shipping;
        }

        file_put_contents("__methods.txt",print_r($methods,true));
        return $methods;
    }

    public function getShippingMethodArr() {
        $activeCarriers = $this->shippingAllmethods->toOptionArray();
        foreach($activeCarriers as $code => $shipping) {
            $methods[$shipping['value']] = $shipping['label'];
        }
        return $methods;
    }
}

