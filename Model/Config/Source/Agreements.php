<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Model\Config\Source;

class Agreements implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Kowal\Cargonizer\Lib\CurlServices               $curlServices,
        \Kowal\Cargonizer\Helper\Config                  $config,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    )
    {
        $this->curlServices = $curlServices;
        $this->config = $config;
        $this->_encryptor = $encryptor;
    }

    public function toOptionArray()
    {
        return $this->getTransportAgreement(); // [['value' => 'Bring', 'label' => __('Bring')], ['value' => 'DB Schenker', 'label' => __('DB Schenker')], ['value' => 'DHL Express', 'label' => __('DHL Express')]];
    }

    public function toArray()
    {
        return ['Bring' => __('Bring'), 'DB Schenker' => __('DB Schenker'), 'DHL Express' => __('DHL Express')];
    }


    private function getTransportAgreement()
    {
        $apiUrl = $this->config->getGeneralCfg('api_url', 0);
        $url = $apiUrl . "/transport_agreements.xml";


        $apiKey = $this->_encryptor->decrypt($this->config->getGeneralCfg('api_key', 0));
        $senderIds = $this->config->getGeneralCfg('sender_ids', 0);

        $methods = [];
        if($result = $this->curlServices
            ->setRequest('GET')
            ->exexute($url, ["X-Cargonizer-Key: {$apiKey}", "X-Cargonizer-Sender: {$senderIds}"])) {

            $z = new \XMLReader;
            $z->XML($result);

            $doc = new \DOMDocument;
            while ($z->read() && $z->name !== 'transport-agreement') ;

            $cariers = [];

            while ($z->name === 'transport-agreement') {
                $node = simplexml_import_dom($doc->importNode($z->expand(), true));
                $carrier = (string)$node->carrier->name;
                foreach ($node->products->children() as $product) {
                    $cariers[$carrier][] = ["value" => (string)$product->identifier, "label" => (string)$product->name];
                }
                $z->next('transport-agreement');
            }
            foreach ($cariers as $carier => $products) {
                $methods[] = ['label' => $carier, 'value' => $products];
            }
        }
        return $methods;
    }

    private function getTransportAgreementArray()
    {
        $apiUrl = $this->config->getGeneralCfg('api_url', 0);
        $url = $apiUrl . "/transport_agreements.xml";


        $apiKey = $this->_encryptor->decrypt($this->config->getGeneralCfg('api_key', 0));
        $senderIds = $this->config->getGeneralCfg('sender_ids', 0);

        $methods = [];
        if($result = $this->curlServices
            ->setRequest('GET')
            ->exexute($url, ["X-Cargonizer-Key: {$apiKey}", "X-Cargonizer-Sender: {$senderIds}"])) {

            $z = new \XMLReader;
            $z->XML($result);

            $doc = new \DOMDocument;
            while ($z->read() && $z->name !== 'transport-agreement') ;

            $cariers = [];

            while ($z->name === 'transport-agreement') {
                $node = simplexml_import_dom($doc->importNode($z->expand(), true));
                $carrier = (string)$node->carrier->name;
                foreach ($node->products->children() as $product) {
                    $cariers[$carrier][] = ["value" => (string)$product->identifier, "label" => (string)$product->name];
                }
                $z->next('transport-agreement');
            }
        }
        return $methods;
    }
}

