<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Model\Config\Source;

class Shipping implements \Magento\Framework\Option\ArrayInterface
{
    protected $scopeConfig;
    protected $shipconfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Shipping\Model\Config $shipconfig,
        \Magento\Shipping\Model\Config\Source\Allmethods $shippingAllmethods
    ) {
        $this->shipconfig = $shipconfig;
        $this->shippingAllmethods = $shippingAllmethods;
        $this->scopeConfig = $scopeConfig;
    }

    public function toOptionArray()
    {
        return $this->getShippingMethods();
    }

    public function toArray()
    {
        return $this->getShippingMethodArr();
    }



    public function getShippingMethods() {

        $activeCarriers = $this->shippingAllmethods->toOptionArray();
        foreach($activeCarriers as $code => $shipping) {

            $methods[] = $shipping;
        }

        file_put_contents("__methods.txt",print_r($methods,true));
        return $methods;
    }

    public function getShippingMethodArr() {
        $activeCarriers = $this->shippingAllmethods->toOptionArray();
        foreach($activeCarriers as $code => $shipping) {
            $methods[$shipping['value']] = $shipping['label'];
        }
        return $methods;
    }
}

