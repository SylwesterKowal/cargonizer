<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Model\ResourceModel\Cargonizer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'cargonizer_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\Cargonizer\Model\Cargonizer::class,
            \Kowal\Cargonizer\Model\ResourceModel\Cargonizer::class
        );
    }
}

