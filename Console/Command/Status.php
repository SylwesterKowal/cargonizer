<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Status extends Command
{

    const ORDER_ID = "name";
    const DISPLAY_ORDERS = "option";

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $name = $input->getArgument(self::ORDER_ID);
        $option = $input->getOption(self::DISPLAY_ORDERS);
        $output->writeln("Hello " . $name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_cargonizer:status");
        $this->setDescription("Update Status");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::DISPLAY_ORDERS, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

