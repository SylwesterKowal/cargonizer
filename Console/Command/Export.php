<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Console\Command;

use Kowal\Cargonizer\Lib\MagentoService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class Export extends Command
{

    const ORDER_ID = "order_id";
    const DISPLAY_ORDERS = "option";

    /**
     * @param \Kowal\Cargonizer\Lib\CurlServices $curlServices
     * @param MagentoService $magentoService
     * @param \Kowal\Cargonizer\Helper\Config $config
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Filesystem\Io\File $file
     * @param \Kowal\Cargonizer\Model\ResourceModel\Cargonizer\CollectionFactory $collectionFactoryCargonizer
     */
    public function __construct(
        \Kowal\Cargonizer\Lib\CurlServices                                 $curlServices,
        \Kowal\Cargonizer\Lib\MagentoService                               $magentoService,
        \Kowal\Cargonizer\Helper\Config                                    $config,
        \Magento\Framework\Encryption\EncryptorInterface                   $encryptor,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory         $collectionFactory,
        \Magento\Framework\Filesystem\DirectoryList                        $directoryList,
        \Magento\Framework\Filesystem\Io\File                              $file,
        \Kowal\Cargonizer\Model\ResourceModel\Cargonizer\CollectionFactory $collectionFactoryCargonizer
    )
    {
        $this->curlServices = $curlServices;
        $this->magentoService = $magentoService;
        $this->config = $config;
        $this->_encryptor = $encryptor;
        $this->collectionFactory = $collectionFactory;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->collectionFactoryCargonizer = $collectionFactoryCargonizer;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected
    function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $order_id = $input->getArgument(self::ORDER_ID);
        $option = $input->getOption(self::DISPLAY_ORDERS);
        $orderStatus = $this->config->getGeneralCfg('order_status', 0);
        $collection = $this->getSalesOrderCollection(['status' => ['eq' => $orderStatus], 'cargonizer' => ['null' => true, 'eq' => '']]);
//        $output->writeln("Order status " . $orderStatus);

        $i = 0;
        foreach ($collection as $order) {
            if (!empty($option) && $option == "-d") {
                $output->writeln("Order " . $order->getId());
                continue;
            }
            if (!empty($order_id) && $order->getId() != $order_id) continue;

            $xml = new \Kowal\Cargonizer\Helper\Xml($order, $this->directoryList, $this->file, $this->magentoService, $this->curlServices, $this->config, $this->collectionFactoryCargonizer);
            if( $contentXml = $xml->execute()) {

                if ($this->export($contentXml, $order->getId())) {
                } else {
                    $output->writeln("Error: " . $order->getId());
                }
            }
            $output->writeln($i." Export " . $order->getId());
            $i++;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected
    function configure()
    {
        $this->setName("kowal_cargonizer:export");
        $this->setDescription("Export Orders");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Order_Id"),
            new InputOption(self::DISPLAY_ORDERS, "-d", InputOption::VALUE_NONE, "Display Order ID")
        ]);
        parent::configure();
    }


    public
    function getSalesOrderCollection(array $filters = [])
    {

        $collection = $this->collectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }

    private
    function export($content, $order_id)
    {
        $apiKey = $this->_encryptor->decrypt($this->config->getGeneralCfg('api_key', 0));
        $apiUrl = $this->config->getGeneralCfg('api_url', 0);
        $senderIds = $this->config->getGeneralCfg('sender_ids', 0);
        $result = $this->curlServices
            ->setRequest('POST')
            ->setDataString($content)
            ->exexute($apiUrl . "/consignments.xml",
                [
                    "X-Cargonizer-Key: {$apiKey}",
                    "X-Cargonizer-Sender: {$senderIds}",
                    "Content-Type: application/xml"
                ]);

        $xml = new \SimpleXMLElement($result);

        if (isset($xml->consignment->number)) {
            echo $xml->consignment->number;
            $this->magentoService->setExported($order_id);
            return true;
        } else {

            throw new \Exception(print_r($result, true));
        }
    }
}

