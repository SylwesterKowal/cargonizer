<?php

namespace Kowal\Cargonizer\Helper;
class Xml extends \Kowal\Cargonizer\Helper\Seller
{
    private $order = null;
    public $seller = null;
    public $directoryList = null;
    public $file = null;

    public function __construct(
        $order,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Io\File $file,
        \Kowal\Cargonizer\Lib\MagentoService $magentoService,
        \Kowal\Cargonizer\Lib\CurlServices $curlServices,
        \Kowal\Cargonizer\Helper\Config $config,
        \Kowal\Cargonizer\Model\ResourceModel\Cargonizer\CollectionFactory $collectionFactoryCargonizer
    )
    {
        $this->order = $order;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->magentoService = $magentoService;
        $this->config = $config;
        $this->collectionFactoryCargonizer = $collectionFactoryCargonizer;
    }

    public function execute()
    {

        try {
            $orderItems = $this->order->getAllItems();


            $payment = $this->order->getPayment();
            $method = $payment->getMethodInstance();
            $paymentTitle = $method->getTitle();
            $paymentCode = $method->getCode();

            $shippingAddress = $this->order->getShippingAddress();
            $person = trim($shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname());
            $company = (!empty($shippingAddress->getCompany())) ? trim($shippingAddress->getCompany()) : "";
            $street = $shippingAddress->getStreet()[0];
            $street = (isset($shippingAddress->getStreet()[1])) ? $street . ' ' . $shippingAddress->getStreet()[1] : $street;
            $vat_id = $shippingAddress->getVatId();


            $billingAddress = $this->order->getBillingAddress();
            $blling_person = trim($billingAddress->getFirstname() . ' ' . $billingAddress->getLastname());
            $blling_company = (!empty($billingAddress->getCompany())) ? trim($billingAddress->getCompany()) : "";
            $blling_street = $billingAddress->getStreet()[0];
            $blling_street = (isset($billingAddress->getStreet()[1])) ? $blling_street . ' ' . $billingAddress->getStreet()[1] : $blling_street;
//            file_put_contents("_order_billing_data.txt",print_r($billingAddress->getData(),true));

            $consignor = $this->config->getGeneralCfg('consignor', 0);


            if (!$ta = $this->getTransportAgreement()) return false;

            $xml = "<consignments>";
            $xml .= "<consignment transport_agreement=\"{$ta['transport_agreement']}\">";
            $xml .= "<transfer>false</transfer>";
            $xml .= "<booking_request>false</booking_request>";
            $xml .= "<email-notification-to-consignee>true</email-notification-to-consignee>";
            $xml .= "<product>{$ta['product']}</product>";
            $xml .= "<parts>";
            $xml .= "<consignee>";
            $xml .= "<name><![CDATA[{$person}]]></name>";
            $xml .= "<postcode>{$shippingAddress->getPostcode()}</postcode>";
            $xml .= "<address1><![CDATA[{$street}]]></address1>";
            $xml .= "<city>{$shippingAddress->getCity()}</city>";
            $xml .= "<country>{$shippingAddress->getCountryId()}</country>";
            $xml .= "<email>{$this->order->getCustomerEmail()}</email>";
            $xml .= "<mobile>{$shippingAddress->getTelephone()}</mobile>";

            $xml .= "<contact-person>{$person}</contact-person>";
            $xml .= "</consignee>";
//        $xml .= "<pickup_address id=\"1\"/>";
            $xml .= "<return_address>";
            $xml .= "<name><![CDATA[{$this->config->getReturnCfg('name', 0)}]]></name>";
            $xml .= "<address1><![CDATA[{$this->config->getReturnCfg('address1', 0)}]]></address1>";
            $xml .= "<postcode>{$this->config->getReturnCfg('postcode', 0)}</postcode>";
            $xml .= "<country>{$this->config->getReturnCfg('country', 0)}</country>";
            $xml .= "</return_address>";
            $xml .= "</parts>";


            $xml .= "<items>";

            $order_item_increment = 0;
            $totla_qty = 0;
            $exported = [];
            foreach ($orderItems as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }

                $sku = $item->getSku();
                $weight = (float)$item->getWeight() * $item->getQtyOrdered();
                $amount = (float)$item->getPrice() * $item->getQtyOrdered();
                $exported[] = $item->getItemId();


                $totla_qty = $totla_qty + $item->getQtyOrdered();
                $order_item_increment++;


            }
            $xml .= "<item type=\"package\" length=\"10\"  height=\"10\" width=\"10\" amount=\"1\" weight=\"1\" volume=\"1\" description=\"\"/>";

            $xml .= "</items>";
            $xml .= "<references>";
            $xml .= "<consignor>{$consignor}</consignor>";
            $xml .= "<consignee>{$this->order->getIncrementId()}</consignee>";
            $xml .= "</references>";
            $xml .= "</consignment>";
            $xml .= "</consignments>";


            $__xml__ = $xml;

            $xml_ = new \SimpleXMLElement($xml);

            $filename = 'order_' . $this->order->getIncrementId() . '.xml';
            $file_path_local = $this->getFileName($filename);
            $xml_->saveXML($file_path_local);
            return $__xml__;

        } catch (Exception $e) {
            throw new \Exception(print_r($e, true));
        }
    }

    /**
     * 21.10.2021, 19:00
     * @param $date
     */
    private function getDate($date)
    {
        $a = explode(",", $date);
        $d = explode(".", $a[0]);
        return $d[2] . '-' . $d[1] . '-' . $d[0];
    }

    private function getTime($date)
    {
        $a = explode(",", $date);
        return trim($a[1]);
    }

    private function getFileName($filename)
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders', 0775);
        }
        return $this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . $filename;
    }


    private function getTransportAgreement()
    {
        $shippingMethodName = $this->order->getShippingDescription();
        $shippingMethodCode = $this->order->getShippingMethod();

        $collection = $this->collectionFactoryCargonizer->create();
        $collection->addFieldToFilter('magento_shipping_method', ['eq' => $shippingMethodCode]);
        $collection->addFieldToSelect('*');
        foreach ($collection as $cargonizer) {
            $grupa_value = $cargonizer->getCargonizerProducts();
            break;
        }
        if (isset($grupa_value) && !empty($grupa_value)) {
            $curier = explode("#", $grupa_value);
            return ['transport_agreement' => $curier[1], 'product' => $curier[0]];
        } else {
            return false;
        }
    }

}
