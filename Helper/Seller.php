<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 27/10/2021
 * Time: 19:39
 */

namespace Kowal\Cargonizer\Helper;

class Seller
{
    public $id = "9471050781002";
    public $vat_id = "9471050781";
    public $codeByBuyer = "CENTAUREA";
    public $accountNumber = "82 1140 2004 0000 3502 8159 1291";
    public $name = "Centrum Edukacyjno-Turystyczne CENTAUREA Wojciech Frycz";
    public $streetAndNumber = "Łagiewnicka 118b";
    public $cityName = "Łódź";
    public $postalCode = "91-471";
    public $country = "PL";
    public $courtAndCapitalInformation = "";
    public $contactInformation = "Wojciech Frycz";
    public $contactPerson = "Wojciech Frycz";
    public $phoneNumbern = "501743286";
    public $fax = "";
    public $electronicMail = "wf@nirko.pl";


}
