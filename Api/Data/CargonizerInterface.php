<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Api\Data;

interface CargonizerInterface
{

    const CARGONIZER_PRODUCTS = 'cargonizer_products';
    const MAGENTO_SHIPPING_METHOD = 'magento_shipping_method';
    const CARGONIZER_ID = 'cargonizer_id';

    /**
     * Get cargonizer_id
     * @return string|null
     */
    public function getCargonizerId();

    /**
     * Set cargonizer_id
     * @param string $cargonizerId
     * @return \Kowal\Cargonizer\Cargonizer\Api\Data\CargonizerInterface
     */
    public function setCargonizerId($cargonizerId);

    /**
     * Get cargonizer_products
     * @return string|null
     */
    public function getCargonizerProducts();

    /**
     * Set cargonizer_products
     * @param string $cargonizerProducts
     * @return \Kowal\Cargonizer\Cargonizer\Api\Data\CargonizerInterface
     */
    public function setCargonizerProducts($cargonizerProducts);

    /**
     * Get magento_shipping_method
     * @return string|null
     */
    public function getMagentoShippingMethod();

    /**
     * Set magento_shipping_method
     * @param string $magentoShippingMethod
     * @return \Kowal\Cargonizer\Cargonizer\Api\Data\CargonizerInterface
     */
    public function setMagentoShippingMethod($magentoShippingMethod);
}

