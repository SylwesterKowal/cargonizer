<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Api\Data;

interface CargonizerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Cargonizer list.
     * @return \Kowal\Cargonizer\Api\Data\CargonizerInterface[]
     */
    public function getItems();

    /**
     * Set cargonizer_products list.
     * @param \Kowal\Cargonizer\Api\Data\CargonizerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

