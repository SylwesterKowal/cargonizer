<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Cargonizer\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CargonizerRepositoryInterface
{

    /**
     * Save Cargonizer
     * @param \Kowal\Cargonizer\Api\Data\CargonizerInterface $cargonizer
     * @return \Kowal\Cargonizer\Api\Data\CargonizerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\Cargonizer\Api\Data\CargonizerInterface $cargonizer
    );

    /**
     * Retrieve Cargonizer
     * @param string $cargonizerId
     * @return \Kowal\Cargonizer\Api\Data\CargonizerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($cargonizerId);

    /**
     * Retrieve Cargonizer matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\Cargonizer\Api\Data\CargonizerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Cargonizer
     * @param \Kowal\Cargonizer\Api\Data\CargonizerInterface $cargonizer
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\Cargonizer\Api\Data\CargonizerInterface $cargonizer
    );

    /**
     * Delete Cargonizer by ID
     * @param string $cargonizerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($cargonizerId);
}

